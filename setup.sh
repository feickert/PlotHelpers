export WORKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PYTHONPATH=$(pwd)/DijetHelpers/scripts:$(pwd)/DijetHelpers/bkgFit:${WORKDIR}:${PYTHONPATH}
#alias prot='python -i prot.py --'
alias prot='${WORKDIR}/prot.sh'
