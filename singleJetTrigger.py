#!/usr/bin/env python

import ROOT
from prot import canvastools
from prot import fittools

trigger_prescales={}
trigger_prescales['HLT_j15']=5130464.578994395
trigger_prescales['HLT_j25']=1363147.0755532861
trigger_prescales['HLT_j35']=300347.2290092829
trigger_prescales['HLT_j60']=26749.761394313336
trigger_prescales['HLT_j110']=2315.408424193459
trigger_prescales['HLT_j175']=312.00144158775277
trigger_prescales['HLT_j260']=48.81502907848251
trigger_prescales['HLT_j380']=1.0000000000310083

def main():
    f=ROOT.TFile.Open('hist.root')

    h=f.Get('HLT_j15/jet0_pt').Clone()
    h.Sumw2()
    h.Reset()

    for trigger,prescale in trigger_prescales.items():
        h_trigger=f.Get(trigger+'/jet0_pt')
        print(trigger,h_trigger.GetBinContent(10),h_trigger.GetBinError(10))
        h.Add(h_trigger, prescale)
    print(h.GetBinContent(10),h.GetBinError(10))

    fittools.fit(h,'pol4',fitrange=(40,150),xrange=(0,200),logy=True,ytitle='Events',xtitle='Leading Jet p_{T} [GeV]',ratiomode='ratio',ratiorange=(-0.5,0.5))
    canvastools.save('singleJetTrigger.pdf')
