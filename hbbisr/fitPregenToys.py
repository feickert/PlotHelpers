from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import utiltools
from prot import fittools

import timeit

import fitdatamctools

from hbbisrnote import constants

import ROOT

import numpy as np

import sys
import array

# ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize")
# ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(1000000)
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
# ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.1)

class FitFunction(ROOT.TPyMultiGenFunction):
    def __init__(self,mchists,datahist,mrange):
        ROOT.TPyMultiGenFunction.__init__(self, self)

        self.datahist=datahist

        self.binmin=datahist.FindBin(mrange[0])
        self.binmax=datahist.FindBin(mrange[1])

        #
        # Pythonize the histogram contents

        # data
        data=[]
        dataerr2=[]
        for binidx in range(self.binmin,self.binmax):
            data.append(datahist.GetBinContent(binidx))
            dataerr2.append(datahist.GetBinError(binidx)*datahist.GetBinError(binidx))
        self.data=np.array(data)
        self.dataerr2=np.array(dataerr2)
        self.data_minbin=datahist.GetBinLowEdge(1)
        self.data_width =datahist.GetBinWidth(1)

        # mc
        self.npar=0
        self.mchists=mchists
        self.mc=np.zeros((len(mchists),self.data.shape[0]))
        mcidx=0
        for mchist in mchists:
            if type(mchist)==tuple:
                self.npar+=1
                mc=[]
                binmin=mchist[0].FindBin(mrange[0])
                binmax=mchist[0].FindBin(mrange[1])
                colidx=0
                for binidx in range(binmin,binmax):
                    for mcsubhist in mchist:
                        self.mc[mcidx,colidx]=mcsubhist.GetBinContent(binidx)
                    colidx+=1
            elif mchist.InheritsFrom(ROOT.TH1.Class()):
                self.npar+=1
                mc=[]
                binmin=mchist.FindBin(mrange[0])
                binmax=mchist.FindBin(mrange[1])
                colidx=0
                for binidx in range(binmin,binmax):
                    self.mc[mcidx,colidx]=mchist.GetBinContent(binidx)
                    colidx+=1
            else:
                self.npar+=mchist.GetNpar()
            mcidx+=1

        # parameters
        self.coeff=np.ones(self.mc.shape[0])

    def NDim(self):
        return self.npar

    def DoEval(self, args):
        # Set any functions and prepare coefficient tables
        argidx=0
        mcidx=0
        for mc in self.mchists:
            if type(mc)==ROOT.TF1:
                for i in range(mc.GetNpar()):
                    mc.SetParameter(i,args[argidx])
                    argidx+=1

                # Histogramize
                for binidx in range(self.binmin,self.binmax):
                    #self.mc[mcidx,binidx-self.binmin]=mc.Eval(self.data_minbin+(binidx-0.5)*self.data_width)
                    self.mc[mcidx,binidx-self.binmin]=mc.Integral(self.data_minbin+(binidx-1)*self.data_width,self.data_minbin+(binidx)*self.data_width)/self.data_width
                mcidx+=1
            else:
                self.coeff[mcidx]=args[argidx]
                argidx+=1
                mcidx+=1

        # calculate chi2
        return (np.square(self.coeff.dot(self.mc)-self.data)/self.dataerr2).sum()

#
# Create a fitter
thisFitter = ROOT.Fit.Fitter()
thisFitter.Config().MinimizerOptions().SetMinimizerType("Minuit2")
thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Minimize")
thisFitter.Config().MinimizerOptions().SetMaxIterations(1000000)
thisFitter.Config().MinimizerOptions().SetMaxFunctionCalls(100000)
thisFitter.Config().MinimizerOptions().SetStrategy(1)
    
def fitDataMC(hs_ms,h_data,fix=None,guess=None,mrange=(60,200)):
    thisFunctor=FitFunction(hs_ms,h_data,mrange)

    # Setup parameters
    a=[12023.6,-229.949,11426.9,-338182]
    a=[50e3,-1.03210e+01,1.61941e+01,-1.37478e+01,4.28395e+00,1.]
    guess=a

    if guess==None: guess=[1.,-1.]+[0.]*(thisFunctor.NDim()-2)
    
    aParams = array.array( 'd', guess) #[1.]*thisFunctor.NDim())
    aSteps  = array.array( 'd', [1e-6]*thisFunctor.NDim())
    thisFitter.Config().SetParamsSettings(thisFunctor.NDim(), aParams, aSteps)

    if fix!=None:
        for parIdx in fix:
            thisFitter.Config().ParamsSettings()[parIdx].Fix()
    
    #
    # Run the fit
    thisFitter.FitFCN(thisFunctor)
    result=ROOT.TFitResult(thisFitter.Result())
    result.Print()
    return result


def main(start,end,mrange=(70,200)):
    #
    # Create histograms
    fh_z    =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-z.root')

    selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77'
    h_z    =fh_z    .Get('{}/Hcand_m_s'.format(selection))

    h_z  .Scale(constants.lumi*1e3)
    print('Expect h_z = {}'.format(h_z.Integral()))

    #f_qcd=ROOT.TF1('fqcd','[0]*exp(-([1]*(x/13000)+[2]*(x/13000)**2+[3]*(x/13000)**3))',0,500)
    f_qcd=ROOT.TF1('fqcd','[0]*exp([1]*(x/200)+[2]*(x/200)**2+[3]*(x/200)**3+[4]*(x/200)**4)',0,500)
    #f_qcd=ROOT.TF1('fqcd','[0]*exp([1]*pow(x,1)+[2]*pow(x,2)+[3]*pow(x,3)+[4]*pow(x,4))',0,500)
    #f_qcd=ROOT.TF1('fqcd','[0]*exp([1]*(x/13000)+[2]*(x/13000)**2+[3]*(x/13000)**3)',0,500)
    #f_qcd=ROOT.TF1('pol3','[0]+[1]*x+[2]*x**2+[3]*x**3',60,250)

    #
    # Load pseudo-data
    #fh_toys=filetools.open('generated_0tag_QCD_with_2tag_Z_template.root')
    fh_toys=filetools.open('filename.root')

    #
    # Run the fits
    fh=open('kktest_{:05d}to{:05d}.csv'.format(start,end-1),'w')
    fh.write('status chi2 ndof muinj mufit mufitunc\n')

    for i in range(start,end):
        h_data=fh_toys.Get('gen_0tag_QCD_2tag_Z_{}__x'.format(i))

        start_time = timeit.default_timer()
        result=fitDataMC([f_qcd,h_z],h_data,mrange=mrange)
        elapsed = timeit.default_timer() - start_time
        print(elapsed)

        chi2=result.MinFcnValue()
        ndof=(h_data.FindBin(mrange[1])-h_data.FindBin(mrange[0]))-1-6

        fh.write('{} {} {} {} {} {}\n'.format(result.Status(),chi2,ndof,1.,result.Parameter(5),result.ParError(5)))
        fh.flush()
        print(result.Parameter(5)*h_z.Integral(h_z.FindBin(mrange[0]),h_z.FindBin(mrange[1])-1))

        # h_qcd=h_data.Clone()
        # utiltools.store.append(h_qcd)
        # h_qcd.Reset()
        # for binidx in range(h_qcd.FindBin(mrange[0]),h_qcd.FindBin(mrange[1])):
        #     val=f_qcd.Integral(h_qcd.GetBinLowEdge(binidx),h_qcd.GetBinLowEdge(binidx+1))/h_qcd.GetBinWidth(binidx)
        #     h_qcd.SetBinContent(binidx,val)
    
        # plottools.plots([(h_qcd,{'title':'QCD',
        #                          'fillcolor':ROOT.UCYellow,
        #                          'opt':'hist'}),
        #                  (h_data,{'title':'Data'})],
        #                 hsopt='nostack',
        #                 text='SR - Dijet+Z Toys',lumi=constants.lumi,sim=True,textpos=(0.5,0.7),
        #                 legend=(0.5,0.93),legendncol=2,
        #                 ytitle='Events',
        #                 xrange=mrange,
        #                 ratiomode='resid')
        # canvastools.save('toyfit_{}to{}.pdf'.format(*mrange))
