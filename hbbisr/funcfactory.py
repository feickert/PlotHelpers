import ROOT

from prot import utiltools

funcs={}

for o in range(1,6):
    name='fexppol{o}'.format(o=o)
    f=ROOT.TF1(name,'[0]*exp(%s)'%('+'.join(['[{i}]*pow(x/200,{i})'.format(i=i) for i in range(1,o+1)])) ,0,500)
    funcs[name]=f
