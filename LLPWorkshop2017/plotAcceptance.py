from prot import filetools
from prot import plottools
from prot import utiltools

import ROOT

def make_acc_plot(inpath):
    f=filetools.open(inpath)

    # make eta acceptance plot
    Rhadron_eta=f.Get('Rhadron_eta')

    total=Rhadron_eta.Integral()

    binc=int(Rhadron_eta.GetNbinsX()/2)+1

    g_hadron=ROOT.TGraph()
    gidx=0
    for i in range(int(Rhadron_eta.GetNbinsX()/2)):
        etacut=Rhadron_eta.GetBinLowEdge(binc+i)

        left =Rhadron_eta.Integral(binc+i,Rhadron_eta.GetNbinsX()+1)
        right=Rhadron_eta.Integral(0,binc-1-i)
        eff=(left+right)/total
        if eff==0: break
        g_hadron.SetPoint(gidx, etacut, eff+0.01)
        gidx+=1

    return g_hadron


def main():
    g_R2000=make_acc_plot('OUT_llp_mc/hist-user.kkrizka.mc15_13TeV.402146.PythiaRhad_AUET2BCTEQ6L1_gen_gluino_p1_2000_qq_100_30ns.20170421-02_EXT3.root')
    g_R1500=make_acc_plot('OUT_llp_mc/hist-user.kkrizka.mc15_13TeV.402112.PythiaRhad_AUET2BCTEQ6L1_gen_gluino_p1_1500_qq_100_30ns.20170421-02_EXT3.root')
    g_R1000=make_acc_plot('OUT_llp_mc/hist-user.kkrizka.mc15_13TeV.402056.PythiaRhad_AUET2BCTEQ6L1_gen_gluino_p1_1000_qq_100_30ns.20170421-02_EXT3.root')
    
    plottools.graphs([(g_R2000,{'title':'m=2~TeV'}),(g_R1500,{'title':'m=1.5~TeV','color':ROOT.kRed}),(g_R1000,{'title':'m=1~TeV','color':ROOT.kBlue})],logy=True,mgopt='AC',legend=(0.2,0.35),ytitle='|#eta| cut')


