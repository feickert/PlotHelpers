import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import style

import selectiontools

from math import *

def cutflow(d_file,**kwargs):
    print(canvastools.savepath)
    cfname=kwargs.get('cfname','default')
    fh=canvastools.fopen('cutflow_%s.tex'%cfname,'w')

    scale=kwargs.get('scale',1)
    h_cutflow=d_file[0].Get('cutflow_weighted')
    for binIdx in range(1,h_cutflow.GetNbinsX()+2):
        if h_cutflow.GetXaxis().GetBinLabel(binIdx) in ['all','']: continue

        cutname='%s%s'%(cfname,h_cutflow.GetXaxis().GetBinLabel(binIdx))
        cutname=cutname.replace('_','')
        cutname=cutname.replace('0','Zero')
        cutname=cutname.replace('1','One')

        nevents=h_cutflow.GetBinContent(binIdx)*scale
        fh.write('\\newcommand{\CF%s}{%d}\n'%(cutname,nevents))
    fh.close()
        
def main(file,**kwargs):
    canvastools.savepath='./plots'
    selectiontools.loop(cutflow,file,**kwargs)
