#!/usr/bin/env python

import ROOT
import glob
import sys
import os, os.path

from prot.filetools import *
from prot.utiltools import *
from prot.plottools import *
from prot.treetools import *
from prot.canvastools import *
from prot.fittools import *
from prot.triggertools import *
from prot.histtools import *
from prot.projectiontools import *

from prot import batchtools

try:
    import __builtin__
except ImportError:
    import builtins
    __builtin__=builtins

hasIPython=hasattr(__builtin__,'__IPYTHON__')

if hasIPython:
    from IPython.core.magic import register_line_magic

if not hasIPython:
    def l(path):
        "line magic for ls'ing ROOT directories"
        thedir=None
        if path=='':
            thedir=ROOT.gDirectory
        else:
            thedir=Get(path)
        contents=[]
        for key in thedir.GetListOfKeys():
            contents.append(key.GetName())
        for name in sorted(contents):
            print(name)

    def c(path):
        "line magic for cd'ing ROOT directories"
        if path=='':
            ROOT.gDirectory.cd()
        elif path=='..':
            ROOT.gDirectory.GetMotherDir().cd()
        else:
            Get(path).cd()
else:
    @register_line_magic
    def l(path):
        "line magic for ls'ing ROOT directories"
        thedir=None
        if path=='':
            thedir=ROOT.gDirectory
        else:
            thedir=Get(path)
        contents=[]
        for key in thedir.GetListOfKeys():
            contents.append(key.GetName())
        for name in sorted(contents):
            print(name)

    @register_line_magic
    def c(path):
        "line magic for cd'ing ROOT directories"
        if path=='':
            ROOT.gDirectory.cd()
        elif path=='..':
            ROOT.gDirectory.GetMotherDir().cd()
        else:
            Get(path).cd()

    # In an interactive session, we need to delete these to avoid
    # name conflicts for automagic to work on line magics.
    del l,c


from math import *

import importlib

import argparse
parser = argparse.ArgumentParser(description="Simple interactive PyROOT")
parser.add_argument('file',nargs='*',help="Input filenames.")
parser.add_argument('-s','--style',action='append',help="Style files to apply.")
parser.add_argument('-b','--batch',action='store_true',help="Run in batch mode.")
parser.add_argument('-c','--cluster',default='local',help="Cluster type (local, condor).")
args = parser.parse_args()

#
# Settings
batchtools.cluster=args.cluster
ROOT.gROOT.SetBatch(args.batch)

#
# Load requested styles
if args.style==None: args.style=[]
for stylepath in args.style:
    style.style.parse(stylepath)

#
# Process argument files
if args.file!=None:
    for path in args.file:
        if path.endswith('.py'):
            # This is a python script. Convert to module notation
            path=path[:-3].replace('/','.')
        
        if path.endswith('.root'): # Open root file
            fm.open(path)
        else: # Execute python script
            script=path.split('(')[0]
            argstr='('+'('.join(path.split('(')[1:]) if len(path.split('('))>1 else '()'
            parts=script.split('.')
            module=None
            pathInModule=[]
            for i in range(len(parts)):
                modname='.'.join(parts[:i+1])
                try:
                    module=importlib.import_module(modname)
                    globals()[modname]=module
                except ImportError:
                    pathInModule.append(parts.pop())
                    break
            if len(pathInModule)==0: # run main
                path=script+'.main'+argstr
            eval(path)

# Exit
if args.batch:
    if hasIPython: exit()
    else: sys.exit()
