from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools
from prot import textools

import ROOT

from hbbisrnote import constants

def plot_samplecomponents(selection,selectiontitle,fh_tex):
    #
    # Higgs
    plottools.plotsf([('ggf:/',{'title':'ggF',
                                'color':ROOT.kYellow+2}),
                      ('vbf:/',{'title':'VBF',
                                'color':ROOT.kMagenta}),
                      ('wh:/',{'title':'WH',
                               'color':ROOT.kRed}),
                      ('zh:/',{'title':'ZH',
                               'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     ytitle='Events',
                     xrange=(0,250),
                     scale=constants.lumi*1e3,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - H #rightarrowb#bar{{b}}'.format(selectiontitle),textpos=(0.2,0.8),lumi=constants.lumi)

    canvastools.save('{}/samplecomponents/higgs/Hcand_m.pdf'.format(selection))

    wh =utiltools.Get('wh:/{}/Hcand_m' .format(selection)).Integral()
    zh =utiltools.Get('zh:/{}/Hcand_m' .format(selection)).Integral()
    ggf=utiltools.Get('ggf:/{}/Hcand_m'.format(selection)).Integral()
    vbf=utiltools.Get('vbf:/{}/Hcand_m'.format(selection)).Integral()
    tot=wh+zh+ggf+vbf

    textools.write_command(fh_tex,'SigComp{}WH' .format(selection), wh/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}ZH' .format(selection), zh/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}VBF'.format(selection),vbf/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}GGF'.format(selection),ggf/tot,':0.2f')

    #
    # ttbar
    plottools.plotsf([('ttbarah:/',{'title':'AllHad',
                                    'color':ROOT.kYellow+2}),
                      ('ttbarsl:/',{'title':'SemiLep',
                                    'color':ROOT.kRed}),
                      ('ttbardl:/',{'title':'DiLep',
                                    'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     ytitle='Events',
                     xrange=(0,250),
                     scale=constants.lumi*1e3,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - t#bar{{t}}'.format(selectiontitle),textpos=(0.2,0.8),lumi=constants.lumi)

    canvastools.save('{}/samplecomponents/ttbar/Hcand_m.pdf'.format(selection))

    ah=utiltools.Get('ttbarah:/{}/Hcand_m'.format(selection)).Integral()
    sl=utiltools.Get('ttbarsl:/{}/Hcand_m'.format(selection)).Integral()
    dl=utiltools.Get('ttbardl:/{}/Hcand_m'.format(selection)).Integral()
    tot=ah+sl+dl

    textools.write_command(fh_tex,'SigComp{}TTbarAH'.format(selection),ah/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}TTbarSL'.format(selection),sl/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}TTbarDL'.format(selection),dl/tot,':0.2f')

    #
    # singletop
    plottools.plotsf([('singletop:/'    ,{'title':'Single Top',
                                          'color':ROOT.kYellow+2,
                                          'fillcolor':None}),
                      ('singleantitop:/',{'title':'Single AntiTop',
                                          'color':ROOT.kMagenta})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     ytitle='Events',
                     xrange=(0,250),
                     scale=constants.lumi*1e3,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - Single Top'.format(selectiontitle),textpos=(0.2,0.8),lumi=constants.lumi)

    canvastools.save('{}/samplecomponents/singletop/Hcand_m.pdf'.format(selection))

    singletop=utiltools.Get('singletop:/{}/Hcand_m' .format(selection)).Integral()
    singleantitop=utiltools.Get('singleantitop:/{}/Hcand_m'.format(selection)).Integral()
    tot=singletop+singleantitop

    textools.write_command(fh_tex,'SigComp{}SingleTop'    .format(selection),    singletop/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}SingleAntiTop'.format(selection),singleantitop/tot,':0.2f')

    #
    # V
    plottools.plotsf([('w:/',{'title':'W+jets',
                               'color':ROOT.kRed}),
                      ('z:/',{'title':'Z+jets',
                               'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',fillcolor=None,
                     ytitle='Events',
                     xrange=(0,250),
                     scale=constants.lumi*1e3,
                     legend=(0.5,0.6),legendncol=1,
                     text='{} - V+jets'.format(selectiontitle),textpos=(0.47,0.8),lumi=constants.lumi,ratio=None)

    canvastools.save('{}/samplecomponents/v/Hcand_m.pdf'.format(selection))

    w=utiltools.Get('w:/{}/Hcand_m' .format(selection)).Integral()
    z=utiltools.Get('z:/{}/Hcand_m'.format(selection)).Integral()
    tot=w+z

    textools.write_command(fh_tex,'SigComp{}VW' .format(selection),w/tot,':0.2f')
    textools.write_command(fh_tex,'SigComp{}VZ' .format(selection),z/tot,':0.2f')

def plot_samplecomponents_normalized(selection,selectiontitle):
    #
    # Higgs
    plottools.plotsf([('ggf:/',{'title':'ggF',
                                'color':ROOT.kYellow+2}),
                      ('vbf:/',{'title':'VBF',
                                'color':ROOT.kMagenta}),
                      ('wh:/',{'title':'WH',
                               'color':ROOT.kRed}),
                      ('zh:/',{'title':'ZH',
                               'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     xrange=(0,250),
                     normalize=True,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - H #rightarrowb#bar{{b}}'.format(selectiontitle),textpos=(0.2,0.8))

    canvastools.save('{}/samplecomponents/higgs/norm/Hcand_m.pdf'.format(selection))

    #
    # ttbar
    plottools.plotsf([('ttbarah:/',{'title':'AllHad',
                                    'color':ROOT.kYellow+2}),
                      ('ttbarsl:/',{'title':'SemiLep',
                                    'color':ROOT.kRed}),
                      ('ttbardl:/',{'title':'DiLep',
                                    'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     xrange=(0,250),
                     normalize=True,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - t#bar{{t}}'.format(selectiontitle),textpos=(0.2,0.8))

    canvastools.save('{}/samplecomponents/ttbar/norm/Hcand_m.pdf'.format(selection))

    #
    # singletop
    plottools.plotsf([('singletop:/'    ,{'title':'Single Top',
                                          'color':ROOT.kYellow+2,
                                          'fillcolor':None}),
                      ('singleantitop:/',{'title':'Single AntiTop',
                                          'color':ROOT.kMagenta})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     xrange=(0,250),
                     normalize=True,
                     legend=(0.2,0.7),legendncol=1,
                     text='{} - Single Top'.format(selectiontitle),textpos=(0.2,0.8))

    canvastools.save('{}/samplecomponents/singletop/norm/Hcand_m.pdf'.format(selection))

    #
    # V
    plottools.plotsf([('w:/',{'title':'W+jets',
                               'color':ROOT.kRed}),
                      ('z:/',{'title':'Z+jets',
                               'color':ROOT.kBlue-4})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',fillcolor=None,
                     xrange=(0,250),
                     normalize=True,
                     legend=(0.5,0.6),legendncol=1,
                     text='{} - V+jets'.format(selectiontitle),textpos=(0.47,0.8),ratio=None)

    canvastools.save('{}/samplecomponents/v/norm/Hcand_m.pdf'.format(selection))
    
def main():
    #
    # File map
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309451.Pythia8EvtGen_A14NNPDF23LO_WH125_bb_fj350.fatjet.20180318-03_tree.root.root','wh')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309452.Pythia8EvtGen_A14NNPDF23LO_ZH125_bb_fj350.fatjet.20180318-03_tree.root.root','zh')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.345338.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb.fatjet.20180318-03_tree.root.root','vbf')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309450.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200.fatjet.20180318-03_tree.root.root','ggf')

    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.fatjet.20180414-01_tree.root.root'   ,'ttbarah')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.fatjet.20180414-01_tree.root.root','ttbarsl')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.fatjet.20180414-01_tree.root.root'      ,'ttbardl')    

    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.fatjet.20180414-01_tree.root.root'    ,'singletop')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.fatjet.20180414-01_tree.root.root','singleantitop')

    filetools.filemap('OUT_fatjet_mc/hist-w.root', 'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root', 'z')
    
    #
    # Plot event counts
    fh_tex=open('signalcomponents.tex','w')
    
    plot_samplecomponents(constants.selectionCR.replace('_hm40',''), 'CRqcd', fh_tex)
    plot_samplecomponents(constants.selectionVR.replace('_hm40',''), 'VR'   , fh_tex)
    plot_samplecomponents(constants.selectionSR.replace('_hm40',''), 'SR'   , fh_tex)
    
    fh_tex.close()

    #
    # Plot normalized
    plot_samplecomponents_normalized(constants.selectionCR.replace('_hm40',''), 'CRqcd')
    plot_samplecomponents_normalized(constants.selectionVR.replace('_hm40',''), 'VR'   )
    plot_samplecomponents_normalized(constants.selectionSR.replace('_hm40',''), 'SR'   )
