from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools
from prot import textools

import dataliketools

import ROOT

from hbbisrnote import constants

def plot_effstat(selection):
    h_evt=utiltools.Get('qcd:/{}/Hcand_m'.format(selection))
    h_eff=dataliketools.get_effective_stats(h_evt)

    plottools.plots([(h_evt,{'title':'Expected',
                             'scale':constants.lumi*1e3,
                             'color':ROOT.kBlack,
                             'fillcolor':None}),
                     (h_eff,{'title':'Effective Statistics',
                             'color':ROOT.kBlue})],
                    hsopt='nostack',opt='hist',
                    ytitle='Events',logy=True,
                    xrange=(0,250),
                    legend=(0.18,0.93),
                    text='Pythia 8 QCD - {}'.format(constants.selectiontitles[selection]),textpos=(0.55,0.8),lumi=constants.lumi,)

    canvastools.save('{}/effstat/Hcand_m.pdf'.format(selection))

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','qcd')

    plot_effstat(constants.selectionSR)
    plot_effstat(constants.selectionCR)
