from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools

import ROOT

from hbbisrnote import constants

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    plottools.plotsf([('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag0in2ext_MV2c10_FixedCutBEff_85',{'title':'CRqcd',
                                                                                                                                              'color':ROOT.kGreen+2}),
                      ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nlnotbtag1in2_MV2c10_FixedCutBEff_77',{'title':'VR (#geq 1 btag)',
                                                                                                                                               'color':ROOT.kBlue}),
                      # ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nlnotbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Alt VR (= 2 btag)',
                      #                                                                                                                             'color':ROOT.kBlue,
                      #                                                                                                                             'linestyle':ROOT.kDashed}),
                      ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'SR',
                                                                                                                                              'color':ROOT.kRed})],
                     'Hcand_m',
                     scale=constants.lumi*1e3,
                     hsopt='nostack',opt='hist',
                     ytitle='Events',logy=True,
                     xrange=(0,250),
                     legend=(0.6,0.9),legendncol=1,
                     text='Pythia 8 QCD',textpos=(0.2,0.8),lumi=constants.lumi)

    canvastools.save('regions/Hcand_m.pdf')

    plottools.plotsf([('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag0in2ext_MV2c10_FixedCutBEff_85',{'title':'CRqcd',
                                                                                                                                              'color':ROOT.kGreen+2}),
                      ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nlnotbtag1in2_MV2c10_FixedCutBEff_77',{'title':'VR (#geq 1 btag)',
                                                                                                                                               'color':ROOT.kBlue}),
                      # ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nlnotbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Alt VR (= 2 btag)',
                      #                                                                                                                             'color':ROOT.kBlue,
                      #                                                                                                                             'linestyle':ROOT.kDashed}),
                      ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'SR',
                                                                                                                                              'color':ROOT.kRed})],
                     'Hcand_m',
                     hsopt='nostack',opt='hist',
                     ytitle='Events',logy=True,
                     xrange=(0,250),
                     ratiorange=(0.8,1.2),ratio=2,
                     normalize=(100,200),
                     legend=(0.6,0.9),legendncol=1,
                     text='Pythia 8 QCD',textpos=(0.2,0.8))

    canvastools.save('regions/norm/Hcand_m.pdf')
    
    # plottools.plotsf([('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag0in2ext_MV2c10_FixedCutBEff_85',{'title':'CR',
    #                                                                                                                                             'color':ROOT.kGreen+2}),
    #                   ('bkg:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'SR',
    #                                                                                                                                           'color':ROOT.kRed})],
    #                  'Hcand_m',
    #                  scale=constants.lumi*1e3,
    #                  fillcolor=None,
    #                  hsopt='nostack',opt='hist',
    #                  normalize=(200,250),
    #                  ytitle='Events',logy=True,
    #                  xrange=(0,250),
    #                  ratiorange=(0.5,1.5),
    #                  legend=(0.6,0.7),legendncol=1,
    #                  text='Pythia 8 QCD',textpos=(0.6,0.8),lumi=constants.lumi)

    # canvastools.save('cr2sr.pdf')
