import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import style

import selectiontools

from hbbisrnote import constants

from math import *

def dosoversqrtb(h_bkg,h_sig,cutdir='<'):
    s=0
    b=0
    h_soversqrtb=h_bkg.Clone()
    h_soversqrtb.Reset()
    h_soversqrtb.GetYaxis().SetTitle('Cumulative s/#sqrt{b}')

    cutrange=range(h_soversqrtb.GetNbinsX()+2) if cutdir=='<' else range(h_soversqrtb.GetNbinsX()+1,-1,-1)
    for binIdx in cutrange:
        s+=h_sig.GetBinContent(binIdx)
        b+=h_bkg.GetBinContent(binIdx)
        if s==0 and b==0: continue
        if b==0: continue
        if b<1e-2: continue
        h_soversqrtb.SetBinContent(binIdx,s/sqrt(b))
    
    return h_soversqrtb;

def runAllCutStudy(d_bkg,d_sig,**kwargs):
    lumi=kwargs.get('lumi',1)
    d_bkg=utiltools.filetag(d_bkg)
    d_sig=utiltools.filetag(d_sig)
    
    #
    # calculate s/sqrt(b)
    rankings=[]

    # The loop over variables
    for key in d_bkg[0].GetListOfKeys():
        if not key.ReadObj().InheritsFrom(ROOT.TH1F.Class()): continue
        if 'cutflow' in key.GetName(): continue
        #if 'nonHcand_m'!=key.GetName(): continue

        varname=key.GetName()

        h_bkg=d_bkg[0].Get('M100to150/{}'.format(key.GetName()))
        h_sig=d_sig[0].Get('M100to150/{}'.format(key.GetName()))
        if h_bkg==None or h_sig==None: continue
        print(key.GetName(),h_bkg,h_sig)
        
        # Calculate s/sqrtb in both directions
        h_soversqrtblt=dosoversqrtb(h_bkg,h_sig,cutdir='<')
        maxsoversqrtblt=h_soversqrtblt.GetMaximum()

        h_soversqrtbgt=dosoversqrtb(h_bkg,h_sig,cutdir='>')
        maxsoversqrtbgt=h_soversqrtbgt.GetMaximum()

        # Pick the more optimal direction
        h_soversqrtb=None
        maxsoversqrtb=None
        if maxsoversqrtblt>maxsoversqrtbgt:
            h_soversqrtb=h_soversqrtblt
            maxsoversqrtb=maxsoversqrtblt
        else:
            h_soversqrtb=h_soversqrtbgt
            maxsoversqrtb=maxsoversqrtbgt

        maxbin=h_soversqrtb.GetMaximumBin()
        cut=h_soversqrtb.GetBinCenter(maxbin)
        sng=h_soversqrtb.GetBinContent(maxbin)*sqrt(lumi*1e3)

        # plot
        if h_bkg.Integral()==0 or h_sig.Integral()==0: continue
        rightmax=1.1*h_soversqrtb.GetMaximum()*sqrt(lumi*1e3)
        leftmax=1.1*max(h_bkg.GetMaximum()/h_bkg.Integral(),h_sig.GetMaximum()/h_sig.Integral())
        rightscale=leftmax/rightmax

        plottools.plots([(h_bkg,{'title':'bkg',
                                 'fillcolor':None}),
                         (h_sig,{'title':'sig',
                                 'linecolor':ROOT.kBlue}),
                         (h_soversqrtb,{'title':'s/#sqrt{b}',
                                        'linestyle':ROOT.kDashed,
                                        'linecolor':ROOT.kViolet,
                                        'scale':sqrt(lumi*1e3)*rightscale,
                                        'normalize':False})],
                        opt='hist',hsopt='nostack',ytitle='a.u.',nounits=True,normalize=True,ratio=None,
                        text='%g GeV < m_{jj} < %g GeV\nOptimal cut: %g\nOptimal s/#sqrt{b}: %g'%(100,150,cut,sng),textpos=(0.55,0.6),
                        legend=(0.75,0.4))
        # plot s/sqrt(b) on twin axis
        c=canvastools.canvas()
        c.cd()
        c.SetTicky(0)

        axis2=ROOT.TGaxis(c.GetUxmax(),0,c.GetUxmax(),c.GetUymax(),0,rightmax,510,"+L")
        axis2.SetLabelColor(ROOT.kViolet)
        axis2.SetLineColor(ROOT.kViolet)
        axis2.Draw()

        utiltools.store.append(axis2)

        canvastools.save(h_soversqrtb.GetName()+'.pdf')

        rankings.append((varname,maxsoversqrtb))

    #
    # save rankings
    rankings=sorted(rankings,key=lambda x: x[1])

    # Add no extra cuts s/sqrt(b)
    bkg_mJ=d_bkg[0].Get('M100to150/Hcand_m')
    sig_mJ=d_sig[0].Get('M100to150/Hcand_m')
    rankings.append(('nocuts',sig_mJ.Integral()/sqrt(bkg_mJ.Integral())))

    fh_r  =open(canvastools.savefullpath(True)+'/rankings.txt','w')
    fh_tex=open(canvastools.savefullpath(True)+'/rankings.tex','w')
    texprefix='SoverSqrtB{}'.format(filetools.objectpath(d_bkg[0]))
    for varname,maxsoversqrtb in rankings:
        significance=maxsoversqrtb*sqrt(lumi*1e3)

        fh_r  .write('%s\t%0.2f\n'%(varname   ,significance))
        textools.write_command(fh_tex, texprefix+varname,significance,fmt=':0.2f')
    fh_r.close()
    fh_tex.close()
        
def main(**kwargs):
    canvastools.savesetup('soversqrtb',**kwargs)

    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    selectiontools.loop(runAllCutStudy,'bkg','sig',selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt*_fj1pt*_tjet2_*sort0_hpt*_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77',lumi=constants.lumi,**kwargs)

    canvastools.savereset()
