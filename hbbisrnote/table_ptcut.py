from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_soversqrtb(ptcut,fh_tex,lumi):
    selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt{ptcut}_fj1pt250_tjet2_d2sort0_hpt{ptcut}_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77'.format(ptcut=ptcut)
    s=utiltools.Get('sig:/{}/M100to150/Hcand_m'.format(selection)).Integral()*lumi*1e3
    b=utiltools.Get('bkg:/{}/M100to150/Hcand_m'.format(selection)).Integral()*lumi*1e3

    sgn=s/sqrt(b)

    print(ptcut,sgn)
    textools.write_command(fh_tex,'Data2017pTCutsoverb{}'.format(ptcut),sgn,fmt=':0.2f')

def main():
    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    fh_tex=open('data2017ptcut.tex','w')

    calc_soversqrtb(460, lumi=77.4,fh_tex=fh_tex)    
    calc_soversqrtb(480, lumi=80.4,fh_tex=fh_tex)

    fh_tex.close()
