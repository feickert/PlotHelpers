from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_btagreleff(tagger,wpnum,wpden,fh_tex):
    print(tagger)
    selection_num='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_{}_{}'.format(tagger,wpnum)
    selection_den='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_hm40_nbtag2in2ext_{}_{}'.format(tagger,wpden)
    num=utiltools.Get('bkg:/{}/M100to150/Hcand_m'.format(selection_num)).Integral()
    den=utiltools.Get('bkg:/{}/M100to150/Hcand_m'.format(selection_den)).Integral()

    ratio=num/den
    print(tagger,wpnum,wpden,ratio,((wpnum/100.)**2/(wpden/100.)**2))
    textools.write_command(fh_tex,'BTagRelEff{}{}ovr{}'.format(tagger,wpnum,wpden),ratio,fmt=':0.2f')

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    fh_tex=open('btagreleff.tex','w')

    calc_btagreleff('MV2c10_FixedCutBEff', 60, 70, fh_tex=fh_tex)
    calc_btagreleff('MV2c10_FixedCutBEff', 70, 77, fh_tex=fh_tex)
    calc_btagreleff('MV2c10_FixedCutBEff', 77, 85, fh_tex=fh_tex)

    fh_tex.close()
