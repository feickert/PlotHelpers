from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_soversqrtb(tagger,fh_tex,lumi):
    print(tagger)
    selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_{}'.format(tagger)
    print(selection)
    s=utiltools.Get('sig:/{}/M100to150/Hcand_m'.format(selection)).Integral()*lumi*1e3
    b=utiltools.Get('bkg:/{}/M100to150/Hcand_m'.format(selection)).Integral()*lumi*1e3

    sgn=s/sqrt(b)
    
    print(selection,sgn)
    textools.write_command(fh_tex,'BTagOptim{}'.format(tagger),sgn,fmt=':0.2f')

def main():
    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    fh_tex=open('btagsoversqrtb.tex','w')

    calc_soversqrtb('MV2c10_FixedCutBEff_60',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_70',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_77',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_85',   lumi=constants.lumi,fh_tex=fh_tex)

    calc_soversqrtb('MV2c10mu_FixedCutBEff_60', lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_70', lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_77', lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_85', lumi=constants.lumi,fh_tex=fh_tex)

    calc_soversqrtb('MV2c10rnn_FixedCutBEff_60',lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_70',lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_77',lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_85',lumi=constants.lumi,fh_tex=fh_tex)

    calc_soversqrtb('DL1_FixedCutBEff_60',      lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_70',      lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_77',      lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_85',      lumi=constants.lumi,fh_tex=fh_tex)

    calc_soversqrtb('DL1mu_FixedCutBEff_60',    lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_70',    lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_77',    lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_85',    lumi=constants.lumi,fh_tex=fh_tex)

    calc_soversqrtb('DL1rnn_FixedCutBEff_60',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_70',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_77',   lumi=constants.lumi,fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_85',   lumi=constants.lumi,fh_tex=fh_tex)

    fh_tex.close()
