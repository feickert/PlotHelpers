from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools
from prot import textools

import ROOT

from hbbisrnote import constants

def plot_samplecomponents(selection,selectiontitle):
    #
    # Resonant
    plottools.plotsf([('v:',{'title':'W+Z',
                             'color':ROOT.kMagenta}),
                      ('h:',{'title':'Higgs',
                             'color':ROOT.kRed}),
                      ('ttbar:',{'title':'t#bar{t}',
                                 'color':ROOT.kAzure+7})],
                     '{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',scale=constants.lumi*1e3,
                     ytitle='Events',
                     xrange=(0,500),
                     legend=(0.6,0.7),legendncol=1,
                     text=selectiontitle,lumi=constants.lumi,textpos=(0.45,0.8))

    canvastools.save('{}/backgroundcomponents/resonant/Hcand_m.pdf'.format(selection))    

    #
    # QCD
    plottools.plot('qcd:/{}/Hcand_m'.format(selection),
                   opt='hist',color=ROOT.kAzure+7,scale=constants.lumi*1e3,
                   ytitle='Events',
                   xrange=(0,500),
                   text='{} - Pythia 8 QCD'.format(selectiontitle),lumi=constants.lumi,textpos=(0.45,0.8))

    canvastools.save('{}/backgroundcomponents/qcd/Hcand_m.pdf'.format(selection))    

    plottools.plot('qcd:/{}/Hcand_m'.format(selection),
                   opt='hist',color=ROOT.kAzure+7,scale=constants.lumi*1e3,
                   ytitle='Events',logy=True,
                   xrange=(0,500),
                   text='{} - Pythia 8 QCD'.format(selectiontitle),lumi=constants.lumi,textpos=(0.25,0.25))

    canvastools.save('{}/backgroundcomponents/qcd_logy/Hcand_m.pdf'.format(selection))    

    #
    # Zprime
    filelist=[]
    i=0
    for Zpmass in constants.Zpmasses:
        color=ROOT.gStyle.GetColorPalette(i)
        i+=1
        filelist.append(('{}:'.format(constants.Zpfile(Zpmass)),{'title':'{} GeV'.format(Zpmass),
                                                                 'color':i}))

    plottools.plotsf(filelist,'{}/Hcand_m'.format(selection),
                     hsopt='nostack',opt='hist',
                     scale=constants.lumi*1e3,
                     xrange=(0,250),
                     ytitle='Events',
                     legend=(0.2,0.9),legendncol=1,
                     text='{} - Z\' g_{{q}}=0.25'.format(selectiontitle),textpos=(0.55,0.8))
    canvastools.save('{}/backgroundcomponents/Zprime/Hcand_m.pdf'.format(selection))
        
def main():
    filetools.filemap('OUT_fatjet_mc/hist-w.root'        ,'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root'        ,'z')
    filetools.filemap('OUT_fatjet_mc/hist-v.root'        ,'v')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'      ,'h')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'      ,'qcd')
    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root'    ,'ttbar')

    plot_samplecomponents(constants.selectionCR.replace('_hm40',''),'CRqcd')
    plot_samplecomponents(constants.selectionVR.replace('_hm40',''),'VR')
    plot_samplecomponents(constants.selectionSR.replace('_hm40',''),'SR')
