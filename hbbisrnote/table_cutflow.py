import ROOT
from prot import utiltools
from prot import filetools
from prot import textools

from hbbisrnote import constants

def generate_table(fpath,selection,selectioncode,scale=1.,fhout=None):
    cutflow=utiltools.Get('{}:/{}/cutflow_weighted'.format(fpath,selection))
    for binidx in range(1,cutflow.GetNbinsX()+2):
        text=cutflow.GetXaxis().GetBinLabel(binidx)
        if text=='': continue
        value=cutflow.GetBinContent(binidx)*scale
        print(text,cutflow.GetBinContent(binidx))
        textools.write_command(fhout, 'Cutflow{}{}'.format(selectioncode,text), value, fmt=':0.0f' )

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'       ,'qcd')
    filetools.filemap('OUT_fatjet_mc/hist-w.root'         ,'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root'         ,'z')
    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root'     ,'ttbar')
    filetools.filemap('OUT_fatjet_mc/hist-singletop.root' ,'singletop')
    filetools.filemap('OUT_fatjet_data/hist-data.root'    ,'data')

    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309450.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200.fatjet.20180318-03_tree.root.root' ,'ggf')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.345338.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb.fatjet.20180318-03_tree.root.root','vbf')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309451.Pythia8EvtGen_A14NNPDF23LO_WH125_bb_fj350.fatjet.20180318-03_tree.root.root'  ,'wh')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309452.Pythia8EvtGen_A14NNPDF23LO_ZH125_bb_fj350.fatjet.20180318-03_tree.root.root'  ,'zh')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'                                                                                                    ,'higgs')

    fh_cutflow=open('cutflow.tex','w')

    generate_table('qcd'      ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdQCD'      ,scale=constants.lumi*1e3*constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdW'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('z'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdZ'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('ttbar'    ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdTTbar'    ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('singletop','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdSingleTop',scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('data'     ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdData'     ,fhout=fh_cutflow)

    generate_table('qcd'      ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdQCD'      ,scale=constants.lumi*1e3*constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdW'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('z'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdZ'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('ttbar'    ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdTTbar'    ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('singletop','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdSingleTop',scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('data'     ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdData'     ,fhout=fh_cutflow)
    
    generate_table('qcd'      ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRQCD'      ,scale=constants.lumi*1e3*constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRW'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('z'        ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRZ'        ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('ttbar'    ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRTTbar'    ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('singletop','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRSingleTop',scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('data'     ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRData'     ,fhout=fh_cutflow)

    generate_table('ggf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdGGF'  ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('vbf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdVBF'  ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('wh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdWH'   ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('zh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdZH'   ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('higgs','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85' ,'CRqcdHiggs',scale=constants.lumi*1e3,fhout=fh_cutflow)

    generate_table('ggf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdGGF'  ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('vbf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdVBF'  ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('wh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdWH'   ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('zh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdZH'   ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('higgs','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77','VRqcdHiggs',scale=constants.lumi*1e3,fhout=fh_cutflow)
    
    generate_table('ggf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRGGF'     ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('vbf'  ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRVBF'     ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('wh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRWH'      ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('zh'   ,'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRZH'      ,scale=constants.lumi*1e3,fhout=fh_cutflow)
    generate_table('higgs','hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77' ,'SRHiggs'   ,scale=constants.lumi*1e3,fhout=fh_cutflow)

    for Zpmass in constants.Zpmasses:
        filestr=constants.Zpfile(Zpmass)

        generate_table(filestr, 'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag0in2ext_MV2c10_FixedCutBEff_85'  ,'CRqcd{}'.format(filestr),scale=constants.lumi*1e3,fhout=fh_cutflow)
        generate_table(filestr, 'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nlnotbtag1in2_MV2c10_FixedCutBEff_77' ,'VRqcd{}'.format(filestr),scale=constants.lumi*1e3,fhout=fh_cutflow)
        generate_table(filestr, 'hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77'  ,'SR{}'.format(filestr),scale=constants.lumi*1e3,fhout=fh_cutflow)        
    
    fh_cutflow.close()
