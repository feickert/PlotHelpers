import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

from hbbisrnote import constants

import copy
import shutil
import copy
import datetime
import os, os.path

from math import *

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'      ,'qcd')

    plottools.plots('qcd:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt250_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77')
