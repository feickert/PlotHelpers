from prot import canvastools
from prot import filetools
from prot import plottools
from prot import utiltools
from prot import textools
from prot import histtools

import ROOT

from hbbisrnote import constants

def plot_truthtagging(selection,categories):
    # Total histogram
    allhists=[]
    for category in categories:
        h_category=utiltools.Get('qcd:/{}/{}/Hcand_m'.format(selection,category))
        if h_category==None: continue
        allhists.append(h_category)
    htotal=histtools.mergestack(allhists)

    # Fraction histograms
    h_categories=[]
    for category in categories:
        h_category=utiltools.Get('qcd:/{}/{}/Hcand_m'.format(selection,category))
        if h_category==None: continue
        h_category=h_category.Clone()
        h_category.Divide(htotal)
        h_categories.append((h_category,{'style':category}))

    # Sort by increasing
    h_categories=sorted(h_categories, key=lambda x: x[0].GetBinContent(x[0].FindBin(125)),reverse=True)

    plottools.plots(h_categories,
                    hsopt='stack',opt='hist',
                    ytitle='Fraction',yrange=(0,1.25),nounits=True,
                    xrange=(0,250),
                    legend=(0.55,0.93),legendncol=5,
                    text='Pythia 8 QCD - {}'.format(constants.selectiontitles[selection]),textpos=(0.2,0.8))

    canvastools.save('{}/truthtagging/Hcand_m.pdf'.format(selection))

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','qcd')

    #plot_truthtagging('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet1ext_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',['BB','CC','LL','BC','BL','CL','B','C','XX'])

    plot_truthtagging('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',['BB','CC','LL','BC','BL','CL','XX'])

    #plot_truthtagging('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2al1_ptsort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',['BB','CC','LL','BC','BL','CL','B','C','XX'])
