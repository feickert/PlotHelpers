import ROOT

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array

from math import *

def TextLabelBold(x,y,text="",color=ROOT.kBlack):
    l = ROOT.TLatex()
    #l.SetNDC()
    l.SetTextFont(62)
    l.SetTextSize(0.045)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)

def bestbins(values):
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins

def main(inLimits,selection='dijetgamma_g130_2j25',lumi=None):
    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    minmR ,maxmR ,nbinsmR =bestbins(mRs)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)    

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl','Exclusion Plot;m_{R} [GeV];g_{SM}',
                 nbinsmR,minmR*1e3,maxmR*1e3,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR*1000,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            h2.SetBinContent(binIdx,xstheory/xslim_obs)
            ratplots[(mR,gSM)]=(xstheory/xslim_obs,xstheory/xslim_exp)

    # Make graph
    gobs3=ROOT.TGraph()
    gexp3=ROOT.TGraph()
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0
    for mR in mRs:
        # Get interesting point
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
        binLim=h.GetNbinsX()
        for binIdx in range(h.GetNbinsX(),0,-1):
            mu=h.GetBinContent(binIdx)
            print('test',binIdx,mu)            
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))

        limit=limits.get(gSMref,{}).get(mR,{})
        if mR==0.55:
            print('GOD',gSMref,mR,limit)
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/xsref)
            gSMexp=sqrt(gSMref**2*xsexp/xsref)

        print(gSMobs,xsobs,xsref)
        print(gSMexp,xsexp,xsref)
        if mR>0.45:
            gobs.SetPoint(gidx,mR*1000,gSMobs)
            gexp.SetPoint(gidx,mR*1000,gSMexp)
            gidx+=1            
        else:
            gobs3.SetPoint(0,mR*1000,gSMobs)
            gexp3.SetPoint(0,mR*1000,gSMexp)
    #gobs.SetPoint(gidx,mRs[-1]*1000+50,gSMobs)
    #gexp.SetPoint(gidx,mRs[-1]*1000+50,gSMexp)

    # Plot plot
    # FIATLAS=ROOT.TColor.CreateGradientColorTable(5,
    #                                              np.array([0.00, 0.16, 0.39, 0.66, 1.00],'d'),
    #                                              np.array([0.51, 1.00, 0.87, 0.00, 0.00],'d'),
    #                                              np.array([0.00, 0.20, 1.00, 0.81, 0.00],'d'),
    #                                              np.array([0.00, 0.00, 0.12, 1.00, 0.51],'d'),
    #                                              255
    #                                              )    
    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )    

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)
    
    c1=canvastools.canvas()
    c1.Clear()

    #l=ROOT.TLegend(0.2,0.3,0.5,0.2)
    l=ROOT.TLegend(0.2,0.82,0.5,0.7)
    l.AddEntry(gobs,'Observed','L')
    l.AddEntry(gexp,'Expected','L')

    #plottools.plot2d(h2,text={'title':selection,'lumi':lumi},textpos=(0.38,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    plottools.plot2d(h2,text={'title':selection,'lumi':lumi,'sim':False},textpos=(0.65,0.67),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')

    Tl=ROOT.TLatex()
    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
        print(mR,gSM)
        Tl.DrawLatex(mR*1000-25,gSM+0.005,'%0.2f'  %(xsobs))
        Tl.DrawLatex(mR*1000-25,gSM-0.015,'(%0.2f)'%(xsexp))

    
    gobs.Draw('C SAME')
    gobs.SetLineWidth(2)
    gobs.SetLineColor(ROOT.kRed)

    gexp.Draw('C SAME')
    gexp.SetLineWidth(2)
    gexp.SetLineColor(ROOT.kRed)
    gexp.SetLineStyle(ROOT.kDashed)

    gobs3.Draw('P SAME')
    gobs3.SetMarkerStyle(ROOT.kFullCircle)
    gobs3.SetMarkerColor(ROOT.kRed)

    gexp3.Draw('P SAME')
    gobs3.SetMarkerStyle(ROOT.kOpenCircle)
    gexp3.SetMarkerColor(ROOT.kRed)

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

    l.Draw()
    l.SetBorderSize(0)

    TextLabelBold(425,0.045,"y*<0.3")
    TextLabelBold(525,0.045,"y*<0.6")

    ar1 = ROOT.TArrow(450,0.035,500,0.035,0.03,"<");
    ar1.Draw("")
    ar1.SetLineWidth(3)
    ar1.SetLineColor(ROOT.kBlack)
    ar2 = ROOT.TArrow(500,0.035,550,0.035,0.03,">");
    ar2.Draw("")
    ar2.SetLineWidth(3)
    ar2.SetLineColor(ROOT.kBlack)

    canvastools.save('test.png')

    
