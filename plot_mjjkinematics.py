from prot import canvastools
from prot import filetools
from prot import plottools

import ROOT
import re

re_nametotitle=re.compile('dijetgamma_g([0-9]+)_2j([0-9]+)_nocuts')

def nametotitle(name):
    match=re_nametotitle.match(name)
    gpt=match.group(1)
    jpt=match.group(2)
    return 'p_{T,#gamma}>%s GeV, p_{T,jets}>%s GeV'%(gpt,jpt)

def main():
    filetools.open('OUT_dijetgamma_data/hist-photontrigger.root')

    plottools.plotsf([('dijetgamma_g150_2j25_nocuts',{'title':nametotitle('dijetgamma_g150_2j25_nocuts')}),
                      ('dijetgamma_g150_2j65_nocuts',{'title':nametotitle('dijetgamma_g150_2j65_nocuts')})],
                      'Zprime_mjj',xrange=(0,500),yrange=(1e3,2e4),logy=True,ratiotitle='Ratio',ratiorange=(0,1),ytitle='Events',lumi=1.9,sim=False,legend=(0.2,0.3),text='#gamma+jet',textpos=(0.55,0.8))
    canvastools.save('mjj_g150.pdf')

    # plottools.plotsf([('dijetgamma_g85_2j25_nocuts',{'title':nametotitle('dijetgamma_g85_2j25_nocuts')}),
    #                   ('dijetgamma_g85_2j65_nocuts',{'title':nametotitle('dijetgamma_g85_2j65_nocuts')})],
    #                   'Zprime_mjj',xrange=(0,500),yrange=(1e3,2e4),logy=True,ratiotitle='Ratio',ratiorange=(0,1),ytitle='Events',lumi=1.9,sim=False,legend=(0.2,0.3),text='#gamma+jet',textpos=(0.55,0.8))
    # canvastools.save('mjj_g85.pdf')

    # plottools.plotsf([('dijetgamma_g85_2j25_nocuts',{'title':nametotitle('dijetgamma_g85_2j25_nocuts')}),
    #                   ('dijetgamma_g150_2j25_nocuts',{'title':nametotitle('dijetgamma_g150_2j25_nocuts')})],
    #                   'Zprime_mjj',xrange=(0,500),yrange=(1e3,2e4),logy=True,ratiotitle='Ratio',ratiorange=(0,1),ytitle='Events',lumi=1.9,sim=False,legend=(0.2,0.3),text='#gamma+jet',textpos=(0.55,0.8))
    # canvastools.save('mjj_2j25.pdf')

    plottools.plotsf([('dijetgamma_g85_2j65_nocuts',{'title':nametotitle('dijetgamma_g85_2j65_nocuts')}),
                      ('dijetgamma_g150_2j65_nocuts',{'title':nametotitle('dijetgamma_g150_2j65_nocuts')})],
                      'Zprime_mjj',xrange=(0,500),yrange=(1e3,2e4),logy=True,ratiotitle='Ratio',ratiorange=(0,1),ytitle='Events',lumi=1.9,sim=False,legend=(0.2,0.3),text='#gamma+jet',textpos=(0.55,0.8))
    canvastools.save('mjj_2j65.pdf')

    plottools.plotsf([('dijetgamma_g85_2j65_nocuts',{'title':nametotitle('dijetgamma_g85_2j65_nocuts')}),
                      ('dijetgamma_g150_2j25_nocuts',{'title':nametotitle('dijetgamma_g150_2j25_nocuts')})],
                      'Zprime_mjj',xrange=(0,500),yrange=(1e3,2e4),logy=True,ratiotitle='Ratio',ratiorange=(0,1),ytitle='Events',lumi=1.9,sim=False,legend=(0.2,0.3),text='#gamma+jet',textpos=(0.55,0.8))
    canvastools.save('mjj_oldnew.pdf')

    plottools.plotsf([('dijetgamma_g85_2j65_nocuts',{'title':nametotitle('dijetgamma_g85_2j65_nocuts')  ,'color':ROOT.kBlack}),
                      ('dijetgamma_g150_2j25_nocuts',{'title':nametotitle('dijetgamma_g150_2j25_nocuts'),'color':ROOT.kRed  }),
                      ('dijetgamma_g150_2j65_nocuts',{'title':nametotitle('dijetgamma_g150_2j65_nocuts'),'color':ROOT.kBlue })],
                      'Zprime_dRjj',hsopt='nostack',opt='hist',ratiotitle='Ratio',ratiorange=(0,2),ratio=1,lumi=1.9,sim=False,legend=(0.16,0.9),text='#gamma+jet',textpos=(0.67,0.8),normalize=True)
    canvastools.save('dRjj.pdf')
