import re
import os, os.path

import ROOT

re_dataset=re.compile('.*\.[0-9]*\.MGPy8EG_N30LO_A14N23LO_dmA_.*_mR([0-9]*p?[0-9]*)_mD([0-9]*p?[0-9]*)_gS([0-9]*p?[0-9]*)_gD([0-9]*p?[0-9]*).*')

def dsname(filename):
    parts=filename.split('.')
    return '.'.join(parts[2:5])

def info(filename):
    match=re_dataset.match(os.path.basename(filename))
    mR =int(float(match.group(1).replace('p','.'))*1000)
    mDM=int(float(match.group(2).replace('p','.'))*1000)
    gSM=float(match.group(3).replace('p','.'))
    gDM=float(match.group(4).replace('p','.'))

    return (mR,mDM,gSM,gDM)

def nameTag(filename):
    if type(filename)!=str and filename.InheritsFrom(ROOT.TFile.Class()):
        filename=os.path.basename(filename.GetFile().GetPath()[0:-2])
    
    mR,mDM,gSM,gDM=info(filename)
    newname = 'ZPrimemR%dgSM%0.2f'%(mR,gSM)
    newname=newname.replace('.','p')

    return newname

re_nametotitle=re.compile('dijetgamma_g([0-9]+)_2j([0-9]+)_.*')
