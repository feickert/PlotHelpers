from prot import canvastools
from prot import filetools
from prot import plottools
from prot import triggertools

import ROOT

import re

re_nametotitle=re.compile('dijetgamma_g([0-9]+)_2j([0-9]+)_.*')

def nametotitle(name):
    match=re_nametotitle.match(name)
    gpt=match.group(1)
    jpt=match.group(2)
    return 'p_{T,#gamma}>%s GeV, p_{T,jets}>%s GeV'%(gpt,jpt)

def plot_trigdatavsmc(selection,variable,**kwargs):
    data=triggertools.trigeff('_file0:/%s/%s'%(selection,variable),'_file0:/%s_ref/%s'%(selection,variable))
    mc  =triggertools.trigeff('_file1:/%s/%s'%(selection,variable),'_file1:/%s_ref/%s'%(selection,variable))
    kwargs['xrange']=(50,150)

    plottools.graphs([(mc.CreateGraph(),{'title':'Sherpa #gamma+jet','color':ROOT.kRed,'marker':ROOT.kOpenCircle}),
                      (data.CreateGraph(),{'title':'Data'})],
                     yrange=(0.9,1.1),legend=(0.6,0.3),
                     text='HLT_g75_tight_3j50noL1_L1EM22VHI\nwrt HLT_g60_loose\n%s'%nametotitle(selection),textpos=(0.2,0.25),
                     **kwargs)

    l=ROOT.TLine(50,1,150,1)
    l.SetLineStyle(ROOT.kDashed)
    l.Draw()
    
    canvastools.save('photontrigger/%s/zoom_%s.pdf'%(selection,variable))
    
def main():
    filetools.open('OUT_photontrigger_data/hist16.root')
    filetools.open('OUT_photontrigger_mc/hist.root')

    plot_trigdatavsmc('dijetgamma_g25_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','photon0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet1_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet2_pt',xrange=(0,100),sim=False,lumi=32.9)
    
    plot_trigdatavsmc('dijetgamma_g25_2j65_HLT_g75_tight_3j50noL1_L1EM22VHI','photon0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_HLT_g75_tight_3j50noL1_L1EM22VHI','jet0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_HLT_g75_tight_3j50noL1_L1EM22VHI','jet1_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_HLT_g75_tight_3j50noL1_L1EM22VHI','jet2_pt',xrange=(0,100),sim=False,lumi=32.9)

    plot_trigdatavsmc('dijetgamma_g85_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','photon0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet1_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_HLT_g75_tight_3j50noL1_L1EM22VHI','jet2_pt',xrange=(0,100),sim=False,lumi=32.9)

    plot_trigdatavsmc('dijetgamma_g25_2j65_thirdjet55_HLT_g75_tight_3j50noL1_L1EM22VHI','photon0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_thirdjet55_HLT_g75_tight_3j50noL1_L1EM22VHI','jet0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_thirdjet55_HLT_g75_tight_3j50noL1_L1EM22VHI','jet1_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g25_2j65_thirdjet55_HLT_g75_tight_3j50noL1_L1EM22VHI','jet2_pt',xrange=(0,100),sim=False,lumi=32.9)
    
    plot_trigdatavsmc('dijetgamma_g85_2j25_trigJetPhotonOverlap_HLT_g75_tight_3j50noL1_L1EM22VHI','photon0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_trigJetPhotonOverlap_HLT_g75_tight_3j50noL1_L1EM22VHI','jet0_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_trigJetPhotonOverlap_HLT_g75_tight_3j50noL1_L1EM22VHI','jet1_pt',xrange=(0,100),sim=False,lumi=32.9)
    plot_trigdatavsmc('dijetgamma_g85_2j25_trigJetPhotonOverlap_HLT_g75_tight_3j50noL1_L1EM22VHI','jet2_pt',xrange=(0,100),sim=False,lumi=32.9)
