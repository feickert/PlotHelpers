import ROOT

from prot import utiltools
from prot import style
from prot import canvastools
from prot import histtools
from prot import systools

import copy

def draw(objs,**kwargs):
    # Convert obj paths to objs
    newobjs=[]
    for obj in objs:
        extrainfo=copy.copy(kwargs)
        if type(obj)==tuple:
            extrainfo.update(obj[1])
            obj=obj[0]
        if type(obj)==str:
            objname=obj
            obj=utiltools.Get(objname)
            if obj==None:
                print('Missing objogram',objname)
                continue
        objclone=obj.Clone()
        objclone.SetDirectory(0)
        utiltools.store.append(objclone)
        newobjs.append((objclone,extrainfo))
    objs=newobjs

    # Apply different styles
    for obj,extrainfo in objs:
        style.style.apply_style(obj,extrainfo,obj.GetName())


    # Draw
    c1=canvastools.canvas()
    #c1.Clear()
    c1.cd()

    for objidx in range(len(objs)):
        obj,extrainfo=objs[objidx]
        extraopt='' if objidx==0 else 'same'
        if 'opt' in extrainfo: extraopt+=extrainfo['opt']
        obj.Draw(extraopt)
    style.style.apply_style(c1,kwargs,'c1')
    canvastools.apply_canvas(c1,kwargs)
    c1.Update()
    c1.Draw()

def plot(hist,**kwargs):
    # Get histogram and clone
    histname=hist
    hist=utiltools.Get(hist)
    if hist==None:
        print('Missing histogram',histname)
        return
    histclone=hist.Clone()
    histclone.SetDirectory(0)    
    utiltools.store.append(histclone)
    orighist=hist
    hist=histclone


    # Rebin
    rebin=style.style.attr(hist.GetName(),kwargs,'rebin')
    if rebin!=None:
        if type(rebin)==int:
            hist.Rebin(rebin)
        else:
            hist=histtools.variable_rebin(hist,rebin)
            utiltools.store.append(hist)

    # Systematics
    sys=kwargs.get('sys',False)
    gsys=None
    opt=''
    syslist=[]
    if sys!=False:
        syslist=systools.get_syslist(orighist,sys,rebin)
        gsys=systools.apply_systematics(hist,syslist.values())
        utiltools.store.append(gsys)
        opt='hist'

    # Draw
    c1=canvastools.canvas()
    c1.Clear()
    style.style.apply_style(hist,kwargs,hist.GetName())

    opt=style.style.attr(hist.GetName(),kwargs,'opt',default=opt)
    hist.Draw(opt)
    if gsys!=None: gsys.Draw("SAME 2")
    style.style.apply_style(c1,style.kwargs_canvas(kwargs),'c1')
    canvastools.apply_canvas(c1,kwargs)
    c1.Draw()

def plots(hists,**kwargs):
    # Convert hist paths to hists
    newhists=[]
    for hist in hists:
        extrainfo=copy.copy(kwargs)
        if type(hist)==tuple:
            extrainfo.update(hist[1])
            hist=hist[0]
        if type(hist)==str:
            histname=hist
            hist=utiltools.Get(histname)
            if hist==None:
                print('Missing histogram',histname)
                continue
        histclone=hist.Clone()
        histclone.SetDirectory(0)
        newhists.append((histclone,extrainfo))
    hists=newhists

    #
    # Prepare default settings

    # Comparing two histograms
    if kwargs.get('default',True):
        if len(hists)==2:
            if 'opt' not in hists[0][1]: hists[0][1]['opt']='hist'
            if 'fillcolor' not in hists[0][1]: hists[0][1]['fillcolor']=ROOT.UCYellowLight
            if 'hsopt' not in kwargs: kwargs['hsopt']='nostack'
            if 'ratio' not in kwargs: kwargs['ratio']=0

    # Prepare stack
    c1=canvastools.canvas()
    c1.Clear()
    dijetard=kwargs.get('dijetard',False)

    # Apply different styles
    for hist,extrainfo in hists:
        # Rebin
        rebin=style.style.attr(hist.GetName(),extrainfo,'rebin')
        if rebin!=None:
            if type(rebin)==int:
                hist.Rebin(rebin)
            else:
                hist=histtools.variable_rebin(hist,rebin)

        # Style
        style.style.apply_style(hist,extrainfo,hist.GetName())

    # Prepare the individual stacks
    hsopt=kwargs.get('hsopt','')
    stackgroups=kwargs.get('stackgroup',range(len(hists)) if 'nostack' in hsopt else [set(range(len(hists)))])
    hstacks=[]
    for stackgroup in stackgroups:
        hstack=None
        if type(stackgroup)==int:
            hist,extrainfo=hists[stackgroup]
            hist.SetOption(extrainfo.get('opt',''))
            hstack=hist
        else:
            hstack=ROOT.THStack()
            for histidx in stackgroup:
                hist,extrainfo=hists[histidx]
                hstack.Add(hist,extrainfo.get('opt',''))
                hstack.SetTitle(';%s;%s'%(hist.GetXaxis().GetTitle(),hist.GetYaxis().GetTitle()))
        utiltools.store.append(hstack)
        hstacks.append(hstack)

    # Fix y-range
    if 'yrange' not in kwargs:
        ymaxs=[]
        ymins=[]
        for hstack in hstacks:
            if hstack.InheritsFrom(ROOT.THStack.Class()):
                hstack=histtools.mergestack(hstack)
            ymaxs.append(hstack.GetMaximum())
            ymins.append(hstack.GetMinimum(0))
        yrange=(0,1.1*max(ymaxs)) if not kwargs.get('logy',False) else (min(ymins),1.1*max(ymaxs)*10)
        kwargs['yrange']=yrange        
        
    # Prepare ratio plot, if requested
    ratioBaseIdx=kwargs.get('ratio',None) if len(hstacks)!=1 else None
    ratiomode=kwargs.get('ratiomode','ratio')
    h_ratios=None
    if ratioBaseIdx!=None:
        h_ratioBase=histtools.mergestack(hstacks[ratioBaseIdx])
        h_ratios=ROOT.THStack()
        h_ratios.SetName(hstack.GetName()+"_ratio")
        utiltools.store.append(h_ratios)

        ratiolist=kwargs.get('ratiolist',range(len(hstacks)))
        for ratioIdx in ratiolist:
            h_ratio=histtools.mergestack(hstacks[ratioIdx]).Clone()
            h_ratios.Add(h_ratio,h_ratio.GetOption())

            if ratiomode=='significance':
                h_ratio.SetFillColor(ROOT.kRed)
                for binIdx in range(h_ratioBase.GetNbinsX()+2):
                    valRef=h_ratioBase.GetBinContent(binIdx)
                    valSec=h_ratio.GetBinContent(binIdx)
                    valErr=h_ratio.GetBinError(binIdx) if h_ratio.GetBinError(binIdx)!=0 else 1.

                    if dijetard:
                        valRef*=h_ratioBase.GetBinWidth(binIdx)
                        valSec*=h_ratioBase.GetBinWidth(binIdx)

                    if valSec!=0:
                        h_ratio.SetBinContent(binIdx,(valRef-valSec)/valErr)
                    else:
                        h_ratio.SetBinContent(binIdx,0)
                    h_ratio.SetBinError(binIdx,0)
                h_ratio.GetYaxis().SetTitle('Significance')
            elif ratiomode=='resid':
                h_ratio.Add(h_ratioBase,-1)
                h_ratio.GetYaxis().SetTitle("HIST-%s"%h_ratioBase.GetTitle())
            else:
                h_ratio.Divide(h_ratioBase)
                h_ratio.GetYaxis().SetTitle("1/%s"%h_ratioBase.GetTitle())

        #Update ratio title object title
        title=h_ratios.GetTitle();
        parts=title.split(';')
        ratiotitle=''
        if ratiomode=='significance':
            ratiotitle='Significance'
        if ratiomode=='resid':
            ratiotitle='HIST-%s'%h_ratioBase.GetTitle()
        else:
            ratiotitle='1/%s'%h_ratioBase.GetTitle() if len(h_ratios.GetHists())!=2 else '#frac{%s}{%s}'%(h_ratios.GetHists()[(ratioBaseIdx+1)%2].GetTitle(),h_ratioBase.GetTitle())
        ratiotitle=kwargs.get('ratiotitle',ratiotitle)
        
        title="%s;%s;%s"%(parts[0],h_ratioBase.GetXaxis().GetTitle(),ratiotitle)
        h_ratios.SetTitle(title)
        h_ratios.GetHists().RemoveAt(ratioBaseIdx)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    pad=c1
    pad_ratio=None
    if h_ratios!=None:
        pad,pad_ratio=canvastools.apply_ratio_canvas_style(c1)
    pad.cd()
    for hstackidx in range(len(hstacks)):
        hstack=hstacks[hstackidx]
        extraopt='' if hstackidx==0 else 'same'
        extraopt+=hstack.GetOption()
        hstack.Draw(extraopt)
        style.style.apply_style(hstack,{'yrange':kwargs['yrange'],'xrange':kwargs.get('xrange',None),'nounits':True},'hstack')

    style.style.apply_style(pad,kwargs,'c1')
    canvastools.apply_canvas(pad,kwargs)
    pad.Update()

    if h_ratios!=None:
        pad_ratio.cd()
        h_ratios.Draw('nostack')
        canvastools.apply_ratio_axis_style(h_ratios)

        if 'ratiorange' in kwargs:
            h_ratios.SetMinimum(kwargs['ratiorange'][0])
            h_ratios.SetMaximum(kwargs['ratiorange'][1])

        h_ratios.GetXaxis().SetRangeUser(pad.GetUxmin(),pad.GetUxmax())

        # reference line
        l=ROOT.TLine(pad.GetUxmin(),1,pad.GetUxmax(),1)
        l.SetLineWidth(2)
        l.DrawClone()

        pad_ratio.Update()

    c1.cd()
    c1.Draw()

def plotsf(paths,hist,**kwargs):
    hists=[]
    for path in paths:
        if type(path)!=tuple: path=(path,{})
        histpath='%s/%s'%(path[0],hist)
        hists.append((histpath,path[1]))
    plots(hists,**kwargs)

def plot2d(hist,**kwargs):
    hist=utiltools.Get(hist).Clone()
    utiltools.store.append(hist)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    opt=kwargs.get('opt','col').lower()
    canvastools.canvasPrepCOLZ(c1,'colz' in opt)
    style.style.apply_style(hist,kwargs,hist.GetName())
    hist.Draw(opt)

    style.style.apply_style(c1,kwargs,'c1')
    canvastools.apply_canvas(c1,kwargs)

    c1.Draw()

def graphs(graphs,**kwargs):
    # Convert hist paths to hists
    newgraphs=[]
    for graph in graphs:
        extrainfo=copy.copy(kwargs)
        if type(graph)==tuple:
            extrainfo.update(graph[1])
            graph=graph[0]
        graphclone=graph.Clone()
        newgraphs.append((graphclone,extrainfo))
    graphs=newgraphs

    # Prepare stack
    mg=ROOT.TMultiGraph()
    utiltools.store.append(mg)

    # Add entries
    for graph,extrainfo in graphs:
        graph.Sort()
        mg.Add(graph,extrainfo.get('opt',''))
        style.style.apply_style(graph,extrainfo,graph.GetName())

        # Copy titles
        mg.SetTitle(';%s;%s'%(graph.GetXaxis().GetTitle(),graph.GetYaxis().GetTitle()))

    # Prepare ratio plot, if requested
    ratioBaseIdx=kwargs.get('ratio',None) if len(graphs)!=1 else None
    mg_ratios=None
    if ratioBaseIdx!=None:
        g_ratioBase=mg.GetListOfGraphs().At(ratioBaseIdx)
        base_xs=g_ratioBase.GetX()
        base_ys=g_ratioBase.GetY()

        mg_ratios=ROOT.TMultiGraph()
        mg_ratios.SetName(mg.GetName()+"_ratio")
        utiltools.store.append(mg_ratios)

        ratiolist=kwargs.get('ratiolist',range(len(mg.GetListOfGraphs())))
        for ratioIdx in ratiolist:
            if ratioIdx==ratioBaseIdx: continue

            g_ratio=mg.GetListOfGraphs().At(ratioIdx).Clone()
            extrainfo=graphs[ratioIdx][1]
            mg_ratios.Add(g_ratio,extrainfo.get('opt',''))

            xs=g_ratio.GetX()
            ys=g_ratio.GetY()
            for gIdx in range(g_ratioBase.GetN()):
                if xs[gIdx]!=base_xs[gIdx]:
                    print("WARNING: Points don't line up for graph ratio!")
                g_ratio.SetPoint(gIdx,xs[gIdx],ys[gIdx]/base_ys[gIdx])


        #Update ratio title object title
        title=mg_ratios.GetTitle();
        parts=title.split(';')
        ratiotitle=''
        ratiotitle=kwargs.get('ratiotitle','Ratio')
        
        title="%s;%s;%s"%(parts[0],g_ratioBase.GetXaxis().GetTitle(),ratiotitle)
        mg_ratios.SetTitle(title)
        #mg_ratios.GetListOfGraphs().RemoveAt(ratioBaseIdx)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    pad=c1
    pad_ratio=None
    if mg_ratios!=None:
        pad,pad_ratio=canvastools.apply_ratio_canvas_style(c1)
        pad_ratio.SetGridy(False)

    pad.cd()
    mg.Draw(kwargs.get('mgopt','AP'))
    style.style.apply_style(mg,kwargs,'mg')
    style.style.apply_style(pad,kwargs,'c1')
    canvastools.apply_canvas(pad,kwargs)
    pad.Update()

    if mg_ratios!=None:
        pad_ratio.cd()
        pad_ratio.SetLogy(True)
        mg_ratios.Draw(kwargs.get('mgopt','AP'))
        canvastools.apply_ratio_axis_style(mg_ratios)

        if 'ratiorange' in kwargs:
            mg_ratios.GetYaxis().SetRangeUser(*kwargs['ratiorange'])

        mg_ratios.GetXaxis().SetLimits(pad.GetUxmin(),pad.GetUxmax())

        pad_ratio.Update()

    c1.cd()
    c1.Draw()
