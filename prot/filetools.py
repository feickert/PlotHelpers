import ROOT

class FileManager:
    def __init__(self):
        self.fhs={}
        self.map={}
        pass

    def open(self,path,mode='READ'):
        # Check if file opened by interpreter
        if(path.startswith("_file")):
            fidx=int(path[5:])
            return ROOT.gROOT.GetListOfFiles().At(fidx)

        # Check remap and open
        finalpath=self.map.get(path,path)

        # Check existing files
        if finalpath in self.fhs:
            if self.fhs[finalpath].IsOpen():
                return self.fhs[finalpath]

        # Must open new!
        fh=ROOT.TFile.Open(finalpath,mode)
        self.fhs[finalpath]=fh
        return fh

    def filemap(self,path,name):
        self.map[name]=path

fm=FileManager()

def open(inpath,mode='READ'):
    path=inpath
    extrainfo=None
    if type(inpath)==tuple:
        path=inpath[0]
        extrainfo=inpath[1]

    fh=path
    if type(path)==str:
        fh=fm.open(path,mode)

    if extrainfo==None:
        return fh
    else:
        return (fh,extrainfo)

def filemap(path,name):
    fm.filemap(path,name)

def objectpath(obj):
    path=''
    if obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
        path=obj.GetPath()
    elif obj.GetDirectory()!=None:
        path='%s/%s'%(obj.GetDirectory().GetPath,obj.GetName())
    return path.split(':')[1]

def mkdir(d,path):
    newd=d.Get(path)
    if newd!=None: return newd
    return d.mkdir(path)
