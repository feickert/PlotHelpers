import ROOT
import array
from math import *

def variable_rebin(hist,newbins):
    newhist=ROOT.TH1F(hist.GetName()+'_rebin',hist.GetTitle(),len(newbins)-1,array.array('d',newbins))
    newhist.GetXaxis().SetTitle(hist.GetXaxis().GetTitle())
    newhist.GetYaxis().SetTitle(hist.GetYaxis().GetTitle())
    for binIdx in range(hist.GetNbinsX()+2):
        x=hist.GetBinCenter(binIdx)
        y=hist.GetBinContent(binIdx)

        newBinIdx=newhist.GetXaxis().FindBin(x)
        newx=newhist.GetBinCenter(newBinIdx)
        newy=newhist.GetBinContent(newBinIdx)

        wratio=newhist.GetBinWidth(newBinIdx)/hist.GetBinWidth(binIdx)

        # Update bin content
        val=newhist.GetBinContent(newBinIdx)+hist.GetBinContent(binIdx)/wratio
        newhist.SetBinContent(newBinIdx,val)

        err=newhist.GetBinError(newBinIdx)
        err=sqrt(err*err+(hist.GetBinError(binIdx)/wratio)*(hist.GetBinError(binIdx)/wratio))
        newhist.SetBinError(newBinIdx,err)

    return newhist

def fixpossionerror(hist):
    for binIdx in range(hist.GetNbinsX()+2):
        hist.SetBinError(binIdx,ROOT.TMath.Sqrt(hist.GetBinContent(binIdx)))

def binrange(hist,xmin=None,xmax=None):
    minBin=hist.FindBin(xmin) if xmin!=None else 1
    maxBin=hist.FindBin(xmax) if xmax!=None else hist.GetNbinsX()+1
    return minBin,maxBin

def getmaximum(hist,xmin=None,xmax=None):
    minBin=hist.FindBin(xmin) if xmin!=None else 1
    maxBin=hist.FindBin(xmax) if xmax!=None else hist.GetNbinsX()+1

    maxdata=None
    maxdatabin=None
    for binIdx in range(minBin,maxBin+1):
        if maxdata==None or hist.GetBinContent(binIdx)>maxdata:
            maxdata   =hist.GetBinContent(binIdx)
            maxdatabin=binIdx
    return maxdatabin,maxdata

def getminimum(hist,xmin=None,xmax=None,above=0):
    minBin=hist.FindBin(xmin) if xmin!=None else 1
    maxBin=hist.FindBin(xmax) if xmax!=None else hist.GetNbinsX()+1

    mindata=0
    mindatabin=None
    for binIdx in range(minBin,maxBin+1):
        val=hist.GetBinContent(binIdx)
        if val>above and (mindatabin==None or val<mindata):
            mindata   =val
            mindatabin=binIdx
    return mindatabin,mindata

def mergestack(hstack):
    if type(hstack)!=list and hstack.InheritsFrom(ROOT.TH1.Class()): return hstack
    if type(hstack)!=list and hstack.InheritsFrom(ROOT.THStack.Class()):
        hs=hstack.GetHists()
        hmerge=hs.At(0).Clone()
    else: # list of TH1
        hs=hstack
        hmerge=hs[0].Clone()

    hmerge.Reset()
    for h in hs:
        hmerge.Add(h,1)
    return hmerge
