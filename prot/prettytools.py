def getminimum(hist,xmin=None,xmax=None,direction='='):
    minBin=hist.FindBin(xmin) if xmin!=None else 1
    maxBin=hist.FindBin(xmax) if xmax!=None else hist.GetNbinsX()

    # Account for direction
    bigBin=hist.GetMaximumBin()
    if direction=='>': minBin=max(minBin,bigBin)
    
    mindata=0
    mindatabin=None
    for binIdx in range(minBin,maxBin+1):
        val=hist.GetBinContent(binIdx)
        if val==0: continue
        if mindatabin==None or val<mindata:
            mindata   =val
            mindatabin=binIdx

    return mindatabin,mindata


def getmaximum(hist,xmin=None,xmax=None):
    minBin=hist.FindBin(xmin) if xmin!=None else 1
    maxBin=hist.FindBin(xmax) if xmax!=None else hist.GetNbinsX()

    maxdata=None
    maxdatabin=None
    for binIdx in range(minBin,maxBin+1):
        if maxdata==None or hist.GetBinContent(binIdx)>maxdata:
            maxdata   =hist.GetBinContent(binIdx)
            maxdatabin=binIdx

    return maxdatabin,maxdata
