import tempfile
import tarfile
import fnmatch
import shutil
import subprocess
import os, os.path

HOST='uct3-lx1'
cluster='local'

def get_batch(name):
    global cluster
    if cluster=='condor':
        return CondorJob(name)
    else:
        return LocalJob(name)

class BatchJob:
    def __init__(self,name):
        self.name=name
        self.commands=[]
        self.data=set()
        self.output=set()
        self.tempdir=None

    def make_package(self):
        self.tempdir=tempfile.mkdtemp(prefix='RUN',dir='batch/')        
        return self.tempdir

    def run(self):
        return None

    def build_command(self,command):
        cmd=None
        args=[]
        kwargs={}
        if type(command)!=tuple:
            cmd=command
        else:
            cmd=command[0]
            for part in command[1:]:
                if type(part)==dict:
                    kwargs.update(part)
                elif type(part)==list:
                    args+=part
        return(cmd,args,kwargs)

    def clean(self):
        if self.tempdir==None: return
        shutil.rmtree(self.tempdir)

class LocalJob(BatchJob):
    def __init__(self,name):
        BatchJob.__init__(self,name)

    def run(self):
        tempdir=self.make_package()

        for command in self.commands:
            cmd,args,kwargs=self.build_command(command)

            cmd(*args,**kwargs)

        return os.getcwd()

class CondorJob(BatchJob):
    def __init__(self,name):
        BatchJob.__init__(self,name)

    def make_package(self):
        tempdir=BatchJob.make_package(self)

        # Make submit files
        fh_dag=open(tempdir+'/condor_prot.dag','w')
        fh_cmd=open(tempdir+'/'+self.name+'.txt','w')

        for command in self.commands:
            cmd,args,kwargs=self.build_command(command)

            #
            sargs=[]
            for arg in args:
                if type(arg)==str:
                    sargs.append('"'+arg+'"')
                else:
                    sargs.append(str(arg))

            #
            skwargs=[]
            for key,value in kwargs.items():
                if type(value)==str:
                    skwargs.append(key+'="'+value+'"')
                else:
                    skwargs.append(key+'='+str(value))

            #
            fh_cmd.write('%s.%s(%s,%s)'%(cmd.__module__,cmd.__name__,','.join(sargs),','.join(skwargs))+'\n')

        nCMD=len(self.commands)
        fh_dag.write('JOB %s condor_prot.submit\n'%self.name)
        fh_dag.write('VARS %s cmdfile="%s.txt" cmdsize="%d" outlist="%s"\n'%(self.name,self.name,nCMD,' '.join(self.output)))

        fh_dag.write('SCRIPT POST %s prot_merge.sh %s\n'%(self.name,' '.join(self.output)))

        fh_dag.close()
        fh_cmd.close()

        # Make data package
        datapath=tempdir+'/data.tar.bz2'
        with tarfile.open(datapath, "w:bz2") as tar:
            for data in self.data:
                tar.add(data)
            tar.close()

        # Make code package
        codepath=tempdir+'/code.tar.bz2'
        with tarfile.open(codepath, "w:bz2") as tar:
            for root,dirs,filenames in os.walk('.'):
                for filename in fnmatch.filter(filenames, '*.py'):
                    tar.add(os.path.join(root, filename))
                for filename in fnmatch.filter(filenames, '*.color'):
                    tar.add(os.path.join(root, filename))
                for filename in fnmatch.filter(filenames, '*.style'):
                    tar.add(os.path.join(root, filename))
                for filename in fnmatch.filter(filenames, '*.C'):
                    tar.add(os.path.join(root, filename))
                for filename in fnmatch.filter(filenames, '*.h'):
                    tar.add(os.path.join(root, filename))
                for filename in fnmatch.filter(filenames, '*.sh'):
                    tar.add(os.path.join(root, filename))
            tar.close()

        # Cleanup
        return tempdir

    def run(self):
        tempdir=self.make_package()

        subprocess.check_call(['scp','-r', tempdir, '%s:~/Zprime/condor'%HOST])
        subprocess.check_call(['ssh',HOST,'~/Zprime/condor/prot_submit.sh %s'%os.path.basename(tempdir)])

        for output in self.output:
            subprocess.check_call(['scp','%s:~/Zprime/condor/%s/%s'%(HOST,os.path.basename(tempdir),output),tempdir])
        return tempdir
