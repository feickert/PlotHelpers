import ROOT

from prot import canvastools
from prot import utiltools
from prot import style

def trigeff(h_pass,h_total,**kwargs):
    h_pass =utiltools.Get(h_pass)
    h_total=utiltools.Get(h_total)

    fitrange=None #(h_total.GetXaxis().GetXmin(),h_total.GetXaxis().GetXmax())
    fitrange=kwargs.get('fitrange',fitrange)

    fermi=None
    if fitrange!=None:
        h_ratio=h_pass.Clone()
        h_ratio.Divide(h_total)
        bin50=h_ratio.FindFirstBinAbove(0.5)
        x50=h_ratio.GetBinCenter(bin50)

        fermi=ROOT.TF1('fermi','[2]/(1.+exp(([0]-x)/[1]))',fitrange[0],fitrange[1])

        fermi.SetParameter(0,x50)
        fermi.SetParameter(1,1)
        fermi.SetParameter(2,1)
        fermi.SetParLimits(2,0,1)
        fermi.SetLineColor(ROOT.kRed)
    
    c1=canvastools.canvas()

    eff = ROOT.TEfficiency(h_pass,h_total);

    # Initial guesses from simple fit
    g=eff.CreateGraph()    
    if fitrange!=None:
        g.Fit(fermi,'BN')
        eff.Fit(fermi,'r')

    style.style.apply_style(g,kwargs,'trigeff')
    g.Draw("AP")
    if fitrange!=None:
        fermi.DrawCopy("SAME")

        xeff=fermi.GetX(0.99)
        print('99% point:',xeff)
    canvastools.apply_canvas(c1,kwargs)
    c1.Update()
    c1.Draw()

    utiltools.store.append(eff)
    return eff
