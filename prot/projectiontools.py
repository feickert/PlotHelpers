import ROOT

from prot import canvastools
from prot import plottools
from prot import utiltools

def projection(hist2d,minval,maxval,**kwargs):
    hist2d=utiltools.Get(hist2d).Clone()

    # Get the projections
    orighist=hist2d.ProjectionY('_py_orig')
    utiltools.store.append(orighist)

    hist=hist2d.ProjectionY('_py',hist2d.GetXaxis().FindBin(minval),hist2d.GetXaxis().FindBin(maxval))
    utiltools.store.append(hist)

    plottools.plots([orighist,hist],**kwargs)

    
