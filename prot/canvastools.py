import ROOT

from prot import utiltools
from prot import legendtools
from prot import style

import os, os.path

savepath='./plots'
savescriptname=''
saveoutname=''
saveextra=''

def canvas(forceNew=False):
    try:
        return ROOT.gPad.GetCanvas()
    except ReferenceError:
        return ROOT.gROOT.MakeDefCanvas()

def apply_canvas(c1,data):
    #
    # Legend!
    if 'legend' in data:
        l=legendtools.make(c1)
        legendtools.add(l,c1.GetListOfPrimitives())
        l.SetNColumns(data.get('legendncol',1))
        if 'legendorder' in data: legendtools.reorder(l,data['legendorder'])
        legendtools.resize(l,pos=style.toposition(data['legend']),width=data.get('legendwidth',0.4))
        l.Draw()

    #
    # Text!
    if 'text' in data:
        pos=(data.get('textpos',(0.2,0.2))[0],data.get('textpos',(0.2,0.2))[1])

        text=data['text']
        sim=data.get('sim',True)
        lumi=data.get('lumi',None)
        suffix=data.get('ATLAS','Internal')
        style.ATLAS(text.split('\n'),xlabel=pos[0],ylabel=pos[1],sim=sim,lumi=lumi,suffix=suffix)

def apply_ratio_canvas_style(c):
    c.Clear()
    c.Divide(1,2)
    pad=c.cd(1)
    pad.SetPad(0.01,0.17,0.99,0.99)
    pad_ratio=c.cd(2)
    pad_ratio.SetBorderSize(0)
    pad_ratio.SetTopMargin(0)
    pad_ratio.SetBottomMargin(0.35)
    pad_ratio.SetPad(0.01,0.0,.99,0.3)
    pad_ratio.SetGridy()

    return pad,pad_ratio

def apply_ratio_axis_style(frame):
    frame.GetXaxis().SetTitleSize(0.17)
    frame.GetXaxis().SetTitleOffset(0.9)

    frame.GetYaxis().SetTitleSize(0.15)
    frame.GetYaxis().SetTitleOffset(0.45)

    frame.GetYaxis().SetNdivisions(5,2,1)

    frame.GetXaxis().SetLabelFont(63);
    frame.GetXaxis().SetLabelSize(14);
    frame.GetYaxis().SetLabelFont(63);
    frame.GetYaxis().SetLabelSize(14);


def set_save_path(path):
    """
    Set the file path to the save directory

    Args:
        path (str): The full file path
    """
    global savepath
    savepath = path


def savesetup(scriptname='', **kwargs):
    global savescriptname,saveoutname

    savescriptname=scriptname
    saveoutname=kwargs.get('outName','')


def save(path, formats=['pdf', 'svg', 'png', 'eps']):
    outpath = savefullpath() + '/' + path
    outdir = os.path.dirname(outpath)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    c1 = canvas()

    if outpath[-4] != '.':
        for outformat in formats:
            c1.SaveAs(outpath + '.' + outformat)
    else:
        for outformat in formats:
            newoutpath = '.'.join(outpath.split('.')[:-1]) + '.' + outformat
            c1.SaveAs(newoutpath)


def savefullpath(mkdir=False):
    global savepath,savescriptname,saveoutname,saveextra

    path=savepath+'/'+savescriptname+'/'+saveoutname+'/'+saveextra

    if mkdir and not os.path.exists(path):
        os.makedirs(path)

    return path

def savereset():
    global savepath,savescriptname,saveoutname,saveextra

    savepath='./plots'
    savescriptname=''
    saveoutname=''
    saveextra=''

def fopen(path,mode='r'):
    global savepath

    outpath=savefullpath()+'/'+path
    outdir=os.path.dirname(outpath)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    return open(outpath,mode)

def canvasPrepCOLZ(pad,isCOLZ=True):
    pad.SetRightMargin(0.15 if isCOLZ else 0.05)
