#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} sample"
    exit -1
fi

SAMPLE=${1}

DATADIR=/global/projecta/projectdirs/atlas/${USER}/batch

rsync -av --delete pdsf.nersc.gov:${DATADIR}/${SAMPLE}/hist*root ${SAMPLE}/
