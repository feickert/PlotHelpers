#!/usr/bin/env python

import fnmatch
import os, sys, time, copy, re
from array import array
from math import sqrt, log, isnan, isinf

from prot import filetools
from prot import canvastools
from prot import plottools
from prot import style
import dijettools

import ROOT
import DijetFuncs
import Minuit2Fit
import singleFit

def plotfits(dataHist,fits,**kwargs):
    hs=[]
    hs.append((dataHist,{'opt':'e'}))

    xrange=kwargs.get('xrange',None)

    hsfits=[]
    for fitName,fitResult in fits:
        fitInfo=DijetFuncs.getFit(fitName)
        fitInfo.TF1.SetParameters(fitResult.GetParams())

        fitHist=dataHist.Clone()
        fitHist.Reset()
        for binIdx in range(1,fitHist.GetNbinsX()+1):
            binLo=fitHist.GetBinLowEdge(binIdx)
            binHi=fitHist.GetBinLowEdge(binIdx+1)
            if xrange!=None and (binLo<xrange[0] or xrange[1]<binHi): continue
            val=fitInfo.TF1.Integral(binLo,binHi)
            fitHist.SetBinContent(binIdx,val)

        hsfits.append((fitHist,
                       {'opt':'hist',
                        'title':fitInfo.fDisplayName,
                        'linecolor':fitInfo.fColor}))
    hs+=sorted(hsfits,key=lambda h: h[1].get('title',h[0].GetTitle()))

    plottools.plots(hs,
                    legend=(0.2,0.5),
                    text=kwargs.get('selection',''),
                    textpos=(0.5,0.75),
                    ytitle='Events',
                    nounits=True,
                    hsopt='nostack',
                    ratio=0,
                    ratiorange=(-5,5),
                    ratiomode='significance',
                    ratiotitle='#frac{Data-Fit}{#sqrt{Fit}}',
                    **kwargs)

def main(infile,massRanges=(200,1500),**kwargs):
    canvastools.savesetup('wilks',**kwargs)

    ### Get input data file ###
    fileIn = filetools.open(infile,'UPDATE')
    if not fileIn:
        print("ERROR, could not find file ", infile)
        exit(1)

    # Process selections
    origsavepath=canvastools.savepath
    dijetdata=dijettools.DijetData(fileIn)
    for selection,lumidatas in dijetdata.selections.items():
        canvastools.savepath=origsavepath+'/'+selection
        for lumi,item in lumidatas.items():
            #if selection not in ['trijet_j430_2j25','dijetgamma_g150_2j25'] or lumi!=0.53: continue
            plotfits(item.h_datalike,item.fitresultLogL.items(),
                     xrange=(min(massRanges),max(massRanges)),
                     logy=True,
                     selection=selection,
                     lumi=lumi,
                     **kwargs)
            canvastools.save('plotSingleFit-%sfb.svg'%(dijettools.lumistr(lumi)))
            
    fileIn.Close()

    canvastools.savereset()
