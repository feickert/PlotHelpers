from prot import systools
from prot import plottools
from prot import style
from prot import canvastools

import zprimetools
import selectiontools

import ROOT

from math import *

def plotAccUncert(*dirs,**kwargs):
    jesnames=['JET_EtaIntercalibration_NonClosure','JET_GroupedNP_1','JET_GroupedNP_2','JET_GroupedNP_3']

    g_sysvsmRs={}
    for dir in dirs:
        mR      =dir[1].get('mR'      ,0)
        scenario=dir[1].get('scenario',1)

        hist_nom=dir[0].Get('Zprime_mjj')
        systs=systools.get_syslist(hist_nom,sys=jesnames)

        #
        # Bin by bin
        gsysts={}
        c=0
        for sysname in systs:
            syst=systs[sysname]
            gsyst=systools.apply_systematics(hist_nom,[syst],symmetric=True)
            gsyst.SetTitle(sysname)
            n=gsyst.GetN()
            xs=gsyst.GetX()
            ys=gsyst.GetY()
            for i in range(gsyst.GetN()):
                gsyst.SetPoint(i,xs[i],0)
            gsysts[sysname]=gsyst
            gsyst.SetFillColor(style.palette[c])
            gsyst.SetMarkerColor(style.palette[c])
            gsyst.SetFillColor(style.palette[c])
            c+=1

        # Acceptance uncertainty
        binMin=hist_nom.FindBin(mR-50)
        binMax=hist_nom.FindBin(mR+50)
        events_nom=hist_nom.Integral(binMin,binMax)

        thesysts={}
        for sysname in systs:
            syst=systs[sysname]
            events_systs=[s.Integral(binMin,binMax) for s in syst]
            thesys=max([abs((events_syst-events_nom)/events_nom) for events_syst in events_systs])
            thesysts[sysname]=thesys

        #
        # Add JES systematics
        jessys=0
        for jesname in jesnames:
            jessys+=thesysts.get(jesname,0)**2
            del thesysts[jesname]
        jessys=sqrt(jessys)
        thesysts['JET_JES_ALL_%d'%scenario]=jessys

        # save to the histogram
        for sysname,thesys in thesysts.items():
            if sysname not in g_sysvsmRs:
                g_sysvsmR=ROOT.TGraph()
                g_sysvsmR.SetTitle('Scenario %d'%scenario)
                style.style.apply_style(g_sysvsmR,{},'scenario%d'%scenario)
                g_sysvsmRs[sysname]=g_sysvsmR
            g_sysvsmR=g_sysvsmRs[sysname]
            g_sysvsmR.SetPoint(g_sysvsmR.GetN(),mR,thesys)


    # plot
    graphs=sorted(g_sysvsmRs.values(),key=lambda x: x.GetTitle())
    plottools.graphs(graphs,opt='APL',xtitle='m_{Z\'} [GeV]',ytitle='(N_{m_{Z\'}#pm50,var}-N_{m_{Z\'}#pm50,nom})/N_{m_{Z\'}#pm50,nom}',legend=(0.2,0.9),yrange=(0,0.15),**kwargs)
    canvastools.save('sysjesscenarios.pdf')
            

        

def main(paths,**kwargs):
    selectiontools.loop(plotAccUncert,*paths,selection='dijetgamma_g130_2j25_nomjj',**kwargs)
