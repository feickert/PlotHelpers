#!/usr/bin/env python

import sys
import csv

if len(sys.argv)!=3:
    print('usage: %s oldformat.txt newformat.csv'%sys.argv[0])
    sys.exit(1)

limits={}
with open(sys.argv[1]) as fh:
    limits=eval(fh.read())

with open(sys.argv[2],'w') as fh:
    writer = csv.DictWriter(fh, fieldnames=['mR','gSM','limexp','limobs','thxsec'], delimiter='\t')
    writer.writeheader()

    for gSM in limits:
        for mR,limit in limits[gSM].items():
            row={'mR':mR,
                 'gSM':gSM,
                 'limexp':limit['exp'],
                 'limobs':limit['obs'],
                 'thxsec':limit['theory']}
            writer.writerow(row)

    
